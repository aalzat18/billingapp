package com.axial.billingcenter.Entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "product")
class Product(@field:ColumnInfo(name = "name") var name: String,
              @field:ColumnInfo(name = "purchase_price") var purchasePrice: Float,
              @field:ColumnInfo(name = "sale_price") var salePrice: Float){

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
    @ColumnInfo(name = "sold_units")
    var soldUnits: Int = 0
}