package com.axial.billingcenter.Entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "debtor")
class Debtor(@field:ColumnInfo(name = "name") var name: String) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
    @ColumnInfo(name = "bill")
    var bill: Float = 0f
    @ColumnInfo(name = "products")
    var products: String = "[]"
}

