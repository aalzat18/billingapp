package com.axial.billingcenter.Helpers

import com.axial.billingcenter.Entities.Debtor
import com.axial.billingcenter.Entities.Product
import com.axial.billingcenter.Models.Person
import com.beust.klaxon.Klaxon

class MapperHelper {

    fun DebtorToPerson(debtor: Debtor): Person {
        var newPerson = Person(
            debtor.id,
            debtor.name,
            debtor.bill,
            transformToModel(debtor.products)
        )

        return newPerson
    }

    fun PersonToDebtor(person: Person): Debtor {
        var newDebtor = Debtor(person.name)
        newDebtor.id = person.id
        newDebtor.bill = person.bill
        newDebtor.products = transformToString(person.products)

        return newDebtor
    }

    private fun transformToModel(jsonString: String): List<Product>? {
        return Klaxon().parseArray(jsonString)
    }

    private fun transformToString(products: List<Product>?): String {
        return Klaxon().toJsonString(products)
    }
}