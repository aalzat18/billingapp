package com.axial.billingcenter.Models

import com.axial.billingcenter.Entities.Product

data class Person (
    val id: Int,
    val name: String,
    var bill: Float,
    var products: List<Product>?
)