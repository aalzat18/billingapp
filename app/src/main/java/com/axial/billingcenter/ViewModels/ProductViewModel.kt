package com.axial.billingcenter.ViewModels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.axial.billingcenter.Entities.Product
import com.axial.billingcenter.Repositories.ProductRepository

class ProductViewModel (application: Application) : AndroidViewModel(application) {
    private val repository: ProductRepository
    val allProducts: LiveData<List<Product>>

    init {
        repository = ProductRepository(application)
        allProducts = repository.allProducts
    }

    fun insert(product: Product) {
        repository.insert(product)
    }

    fun deleteAllProducts() {
        repository.deleteAllProducts()
    }

    fun updateProduct(product: Product) {
        repository.updateProduct(product)
    }
}