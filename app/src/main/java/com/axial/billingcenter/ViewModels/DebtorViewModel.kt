package com.axial.billingcenter.ViewModels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.axial.billingcenter.Entities.Debtor
import com.axial.billingcenter.Repositories.DebtorRepository

class DebtorViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: DebtorRepository
    val allDebtors: LiveData<List<Debtor>>

    init {
        repository = DebtorRepository(application)
        allDebtors = repository.allDebtors
    }

    fun insert(debtor: Debtor) {
        repository.insert(debtor)
    }

    fun deleteAllDebtors() {
        repository.deleteAllDebtors()
    }

    fun updateDebtor(debtor: Debtor) {
        repository.updateDebtor(debtor)
    }
}
