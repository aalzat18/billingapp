package com.axial.billingcenter.Repositories

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.axial.billingcenter.Daos.ProductDao
import com.axial.billingcenter.Data.DebtorDataBase
import com.axial.billingcenter.Entities.Product

class ProductRepository (application: Application) {

    private val productDao: ProductDao
    val allProducts: LiveData<List<Product>>

    init {
        val dataBase = DebtorDataBase.getInstance(application)
        productDao = dataBase.productDao()
        allProducts = productDao.getAllProducts()
    }

    fun insert(product: Product) {
        InsertProductAsyncTask(productDao).execute(product)
    }

    fun getSoldUnidsById(id: Int) {

    }

    fun deleteAllProducts() {
        DeleteAllProductsAsyncTask(productDao).execute()
    }

    fun updateProduct(product: Product) {
        UpdateProductAsyncTask(productDao).execute(product)
    }

    private class InsertProductAsyncTask internal constructor(private val productDao: ProductDao) :
        AsyncTask<Product, Void, Void>() {
        override fun doInBackground(vararg products: Product): Void? {
            productDao.insert(products[0])
            return null
        }
    }

    private class DeleteAllProductsAsyncTask internal constructor(private val productDao: ProductDao) :
        AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg p0: Void?): Void? {
            productDao.deleteAllProducts()
            return null
        }
    }

    private class UpdateProductAsyncTask internal constructor(private val productDao: ProductDao) :
        AsyncTask<Product, Void, Void>() {
        override fun doInBackground(vararg products: Product): Void? {
            productDao.updateProduct(products[0])
            return null
        }
    }
}