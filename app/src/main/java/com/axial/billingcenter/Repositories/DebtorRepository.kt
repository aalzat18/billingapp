package com.axial.billingcenter.Repositories

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.axial.billingcenter.Daos.DebtorDao
import com.axial.billingcenter.Data.DebtorDataBase
import com.axial.billingcenter.Entities.Debtor

class DebtorRepository(application: Application) {

    private val debtorDao: DebtorDao
    val allDebtors: LiveData<List<Debtor>>

    init {
        val dataBase = DebtorDataBase.getInstance(application)
        debtorDao = dataBase.debtorDao()
        allDebtors = debtorDao.getAll()
    }

    fun insert(debtor: Debtor) {
        InsertDebtorAsyncTask(debtorDao).execute(debtor)
    }

    fun deleteAllDebtors() {
        DeleteAllDebtorsAsyncTask(debtorDao).execute()
    }

    fun updateDebtor(debtor: Debtor) {
        UpdateDebtorAsyncTask(debtorDao).execute(debtor)
    }

    private class InsertDebtorAsyncTask internal constructor(private val debtorDao: DebtorDao) :
        AsyncTask<Debtor, Void, Void>() {

        override fun doInBackground(vararg debtors: Debtor): Void? {
            debtorDao.insert(debtors[0])
            return null
        }
    }

    private class DeleteAllDebtorsAsyncTask internal constructor(private val debtorDao: DebtorDao) :
        AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg p0: Void?): Void? {
            debtorDao.deleteAllDebtors()
            return null
        }
    }

    private class UpdateDebtorAsyncTask internal constructor(private val debtorDao: DebtorDao) :
        AsyncTask<Debtor, Void, Void>() {

        override fun doInBackground(vararg debtors: Debtor): Void? {
            debtorDao.updateDebtor(debtors[0])
            return null
        }
    }
}
