package com.axial.billingcenter.BottomSheets

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.axial.billingcenter.Adapters.ProductBottomSheetAdapter
import com.axial.billingcenter.Entities.Debtor
import com.axial.billingcenter.Entities.Product
import com.axial.billingcenter.Helpers.MapperHelper
import com.axial.billingcenter.R
import com.axial.billingcenter.ViewModels.DebtorViewModel
import com.axial.billingcenter.ViewModels.ProductViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class ProductBottomSheet(debtor: Debtor) : BottomSheetDialogFragment() {

    private lateinit var productViewModel: ProductViewModel
    private lateinit var debtorViewModel: DebtorViewModel

    private var products = emptyList<Product>()
    private var debtor = debtor

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.product_list_bottomsheet, container, false)
        val adapter = ProductBottomSheetAdapter(context!!.applicationContext)
        val recyclerView = view.findViewById<RecyclerView>(R.id.recyclerview)
        recyclerView.layoutManager = LinearLayoutManager(context?.applicationContext)
        recyclerView.adapter = adapter

        productViewModel = ViewModelProviders.of(this).get(ProductViewModel::class.java)
        debtorViewModel = ViewModelProviders.of(this).get(DebtorViewModel::class.java)
        productViewModel.allProducts.observe(this, Observer { debtor ->
            debtor?.let {
                adapter.setProducts(it)
            }
        })

        adapter.onItemClick = {
            var alertDialog = AlertDialog.Builder(activity)
            alertDialog.setMessage("¿Deseas agregarle un(a) " + it.name + " de $" + it.salePrice.toInt() + " a " + debtor.name)
            alertDialog.setPositiveButton("Aceptar") { dialogInterface: DialogInterface, i: Int ->
                addProduct(debtor, it)
            }
            alertDialog.setNegativeButton("Cancelar"){ dialogInterface: DialogInterface, i: Int -> }
            alertDialog.show()
        }

        return view
    }

    fun addProduct(debtor: Debtor, currentProduct: Product) {

        currentProduct.soldUnits += 1

        var person = MapperHelper().DebtorToPerson(debtor)

        var newProductList = person.products!!.toMutableList()

        var debtorProduct = Product(currentProduct.name, currentProduct.purchasePrice, currentProduct.salePrice)
        debtorProduct.id = currentProduct.id
        debtorProduct.soldUnits = 1

        for (product in person.products!!) {
            if (product.id == currentProduct.id) {
                debtorProduct.soldUnits = product.soldUnits + 1
                newProductList.remove(product)
            }
        }

        newProductList.add(debtorProduct)
        person.products = newProductList
        person.bill += currentProduct.salePrice

        var updatedDebtor = MapperHelper().PersonToDebtor(person)
        debtorViewModel.updateDebtor(updatedDebtor)
        productViewModel.updateProduct(currentProduct)
    }
}