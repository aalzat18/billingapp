package com.axial.billingcenter.Data

import android.content.Context
import android.os.AsyncTask
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.axial.billingcenter.Daos.DebtorDao
import com.axial.billingcenter.Daos.ProductDao
import com.axial.billingcenter.Entities.Debtor
import com.axial.billingcenter.Entities.Product

@Database(entities = [Debtor::class, Product::class], version = 1)
abstract class DebtorDataBase : RoomDatabase() {

    abstract fun debtorDao(): DebtorDao
    abstract fun productDao(): ProductDao

    private class PopulateDBAsynctask internal constructor(debtorDataBase: DebtorDataBase) :
        AsyncTask<Void, Void, Void>() {
        private val debtorDao: DebtorDao
        private val productDao: ProductDao

        init {
            debtorDao = debtorDataBase.debtorDao()
            productDao = debtorDataBase.productDao()
        }

        override fun doInBackground(vararg voids: Void): Void? {
            debtorDao.insert(Debtor("Hola"))
            productDao.insert(Product("Hola", 1000f, 500f))
            return null
        }
    }

    companion object {

        private var instance: DebtorDataBase? = null

        @Synchronized
        fun getInstance(context: Context): DebtorDataBase {
            if (instance == null)
                instance = Room.databaseBuilder(context.applicationContext, DebtorDataBase::class.java, "debtor")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallBack)
                    .build()

            return instance as DebtorDataBase
        }

        private val roomCallBack = object : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                PopulateDBAsynctask(instance!!).execute()
            }
        }
    }
}
