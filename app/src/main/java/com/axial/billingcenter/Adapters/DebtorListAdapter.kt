package com.axial.billingcenter.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.axial.billingcenter.Entities.Debtor
import com.axial.billingcenter.R

class DebtorListAdapter internal constructor(context: Context) : RecyclerView.Adapter<DebtorListAdapter.DebtorViewHolder>() {

    var onAddProductClick: ((Debtor) -> Unit)? = null
    var onItemClick: ((Debtor) -> Unit)? = null

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var debtors = emptyList<Debtor>()

    inner class DebtorViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val debtorNameView: TextView = itemView.findViewById(R.id.debtorName)
        val debtValueView: TextView = itemView.findViewById(R.id.debtValue)
        val firstLetterNameView: TextView = itemView.findViewById(R.id.firstLetterName)
        init {
            itemView.findViewById<ImageView>(R.id.addProductButton).setOnClickListener {
                onAddProductClick?.invoke(debtors[adapterPosition])
            }

            itemView.findViewById<CardView>(R.id.card_container).setOnClickListener {
                onItemClick?.invoke(debtors[adapterPosition])
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DebtorViewHolder {
        val itemView = inflater.inflate(R.layout.debtor_item, parent, false)
        return DebtorViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: DebtorViewHolder, position: Int) {
        val current = debtors[position]
        holder.debtorNameView.text = current.name
        holder.debtValueView.text = "$" + current.bill.toInt().toString()
        holder.firstLetterNameView.text = current.name[0].toString().toUpperCase()
    }

    fun setDebtors(debtors: List<Debtor>){
        this.debtors = debtors
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return debtors.size
    }
}