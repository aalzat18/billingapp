package com.axial.billingcenter.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.axial.billingcenter.Entities.Product
import com.axial.billingcenter.R

class BillAdapter internal constructor(context: Context) : RecyclerView.Adapter<BillAdapter.BillViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var products : List<Product>? = emptyList()

    inner class BillViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val productNameView: TextView = itemView.findViewById(R.id.textViewNombre)
        val purchasePriceView: TextView = itemView.findViewById(R.id.textViewPurchasePrice)
        val salePriceView: TextView = itemView.findViewById(R.id.textViewSalePrice)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BillViewHolder {

        val itemView = inflater.inflate(R.layout.product_item, parent, false)
        return BillViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return products!!.size
    }

    override fun onBindViewHolder(holder: BillViewHolder, position: Int) {
        val current = products!![position]
        holder.productNameView.text = current.name
        holder.purchasePriceView.text = "x"  + current.soldUnits
        holder.salePriceView.text ="$" + (current.salePrice.toInt() * current.soldUnits)
    }

    fun setProducts(products: List<Product>?){
        this.products = products
        notifyDataSetChanged()
    }
}