package com.axial.billingcenter.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.axial.billingcenter.Entities.Product
import com.axial.billingcenter.R

class ProductBottomSheetAdapter internal constructor(context: Context) : RecyclerView.Adapter<ProductBottomSheetAdapter.ProductBottomSheetViewHolder>() {

    var onItemClick: ((Product) -> Unit)? = null

    private val inlfater: LayoutInflater = LayoutInflater.from(context)
    private var products = emptyList<Product>()

    inner class ProductBottomSheetViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val productNameView: TextView = itemView.findViewById(R.id.textViewNombre)
        val salePriceView: TextView = itemView.findViewById(R.id.textViewSalePrice)
        init {
            itemView.findViewById<LinearLayout>(R.id.product_container).setOnClickListener {
                onItemClick?.invoke(products[adapterPosition])
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductBottomSheetViewHolder {
        val itemView = inlfater.inflate(R.layout.product_bottomsheet_item, parent, false)
        return ProductBottomSheetViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return products.size
    }

    override fun onBindViewHolder(holder: ProductBottomSheetViewHolder, position: Int) {
        val current = products[position]
        holder.productNameView.text = current.name
        holder.salePriceView.text ="$" + current.salePrice.toInt().toString()
    }

    fun setProducts(products: List<Product>){
        this.products = products
        notifyDataSetChanged()
    }
}