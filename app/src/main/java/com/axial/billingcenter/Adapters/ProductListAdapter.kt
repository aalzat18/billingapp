package com.axial.billingcenter.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.axial.billingcenter.Entities.Product
import com.axial.billingcenter.R

class ProductListAdapter internal constructor(context: Context) : RecyclerView.Adapter<ProductListAdapter.ProductViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var products = emptyList<Product>()

    inner class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val productNameView: TextView = itemView.findViewById(R.id.textViewNombre)
        val purchasePriceView: TextView = itemView.findViewById(R.id.textViewPurchasePrice)
        val salePriceView: TextView = itemView.findViewById(R.id.textViewSalePrice)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val itemView = inflater.inflate(R.layout.product_item, parent, false)
        return ProductViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ProductListAdapter.ProductViewHolder, position: Int) {
        val current = products[position]
        holder.productNameView.text = current.name
        holder.purchasePriceView.text = "$" + current.purchasePrice.toInt().toString()
        holder.salePriceView.text ="$" + current.salePrice.toInt().toString()
    }

    override fun getItemCount(): Int {
        return products.size
    }

    fun setProducts(products: List<Product>){
        this.products = products
        notifyDataSetChanged()
    }
}