package com.axial.billingcenter.Activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.axial.billingcenter.R

class AddDebtorActivity : AppCompatActivity() {

    var name : EditText? = null
    var addButtonContainer: FrameLayout? = null
    var addButton: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_debtor_dialog)

        this.name = findViewById(R.id.edit_text_name)
        this.addButtonContainer = findViewById(R.id.frameLayout_add_debtor_container)
        this.addButton = findViewById(R.id.button_add_debtor)

        this.addButton!!.setOnClickListener{
            button()
        }

        this.addButtonContainer!!.setOnClickListener {
            button()
        }
    }

    private fun button() {
        val intent = Intent()
        if (TextUtils.isEmpty(name?.text)) {
            Toast.makeText(
                applicationContext,
                "Nombre en blanco",
                Toast.LENGTH_LONG).show()
        } else {
            val word = name?.text.toString()
            intent.putExtra("name", word)
            setResult(Activity.RESULT_OK, intent)
        }
        finish()
    }
}