package com.axial.billingcenter.Activities

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.axial.billingcenter.Adapters.ProductListAdapter
import com.axial.billingcenter.Entities.Product
import com.axial.billingcenter.R
import com.axial.billingcenter.ViewModels.ProductViewModel

import kotlinx.android.synthetic.main.activity_product.*

class ProductActivity : AppCompatActivity() {

    private lateinit var productViewModel: ProductViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product)
        setSupportActionBar(toolbar)

        val deleteProductIcon = findViewById<ImageView>(R.id.deleteProductsIcon)
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
        val adapter = ProductListAdapter(this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        productViewModel = ViewModelProviders.of(this).get(ProductViewModel::class.java)
        productViewModel.allProducts.observe(this, Observer { product ->
            product?.let {
                adapter.setProducts(it)
            }
        })

        fab.setOnClickListener { view ->
            val intent = Intent(this@ProductActivity, AddProductActivity::class.java)
            startActivityForResult(intent, 1)
        }

        deleteProductIcon.setOnClickListener { view ->
            var alertDialog = AlertDialog.Builder(this)
            alertDialog.setTitle("¡Advertencia!")
            alertDialog.setMessage("¿Desea eliminar todo el listado de productos?")
            alertDialog.setPositiveButton("Aceptar") { dialogInterface: DialogInterface, i: Int ->
                productViewModel.deleteAllProducts()
            }
            alertDialog.setNegativeButton("Cancelar"){ dialogInterface: DialogInterface, i: Int -> }
            alertDialog.show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            data?.let {
                val product = Product(it.getStringExtra("name"),
                    it.getFloatExtra("purchasePrice", 0f),
                    it.getFloatExtra("salePrice", 0f))
                productViewModel.insert(product)
            }
        }
    }
}
