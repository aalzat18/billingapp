package com.axial.billingcenter.Activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.axial.billingcenter.R

class AddProductActivity : AppCompatActivity() {

    var name: EditText? = null
    var purchasePrice: EditText? = null
    var salePrice: EditText? = null
    var addButtonContainer: FrameLayout? = null
    var addButton: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_product_dialog)

        this.name = findViewById(R.id.edit_text_product_name)
        this.purchasePrice = findViewById(R.id.edit_text_purchase_price)
        this.salePrice = findViewById(R.id.edit_text_sale_price)
        this.addButtonContainer = findViewById(R.id.frameLayout_add_debtor_container)
        this.addButton = findViewById(R.id.button_add_debtor)

        this.addButton!!.setOnClickListener{
            button()
        }

        this.addButtonContainer!!.setOnClickListener {
            button()
        }
    }

    private fun button() {
        val intent = Intent()
        if (TextUtils.isEmpty(name?.text)) {
            Toast.makeText(
                applicationContext,
                "Favor llenar todos los campos",
                Toast.LENGTH_LONG).show()
        } else {
            val productName = name?.text.toString()
            val productPurchasePrice = purchasePrice?.text.toString().toFloat()
            val productSalePrice = salePrice?.text.toString().toFloat()
            intent.putExtra("name", productName)
            intent.putExtra("purchasePrice", productPurchasePrice)
            intent.putExtra("salePrice", productSalePrice)
            setResult(Activity.RESULT_OK, intent)
        }
        finish()
    }
}