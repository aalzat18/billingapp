package com.axial.billingcenter.Activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.axial.billingcenter.Adapters.DebtorListAdapter
import com.axial.billingcenter.BottomSheets.ProductBottomSheet
import com.axial.billingcenter.Entities.Debtor
import com.axial.billingcenter.R
import com.axial.billingcenter.ViewModels.DebtorViewModel
import com.beust.klaxon.Klaxon

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var debtorViewModel: DebtorViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
        val adapter = DebtorListAdapter(this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter.onAddProductClick = {
            val bottomSheet = ProductBottomSheet(it)
            bottomSheet.show(supportFragmentManager, "BottomSheetEx")
        }

        adapter.onItemClick = {
            val intent = Intent(this@MainActivity, BillActivity::class.java)
            intent.putExtra("debtor", Klaxon().toJsonString(it))
            startActivity(intent)
        }

        debtorViewModel = ViewModelProviders.of(this).get(DebtorViewModel::class.java)
        debtorViewModel.allDebtors.observe(this, Observer { debtor ->
            debtor?.let {
                adapter.setDebtors(it)
                if (it.isNotEmpty()){
                    recyclerView.visibility = View.VISIBLE
                    empty.visibility = View.GONE
                }
            }
        })

        fab.setOnClickListener { view ->
            val intent = Intent(this@MainActivity, AddDebtorActivity::class.java)
            startActivityForResult(intent, 1)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            data?.let {
                val debtor = Debtor(it.getStringExtra("name"))
                debtorViewModel.insert(debtor)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> {
                debtorViewModel.deleteAllDebtors()
                findViewById<RecyclerView>(R.id.recyclerview).visibility = View.GONE
                empty.visibility = View.VISIBLE
                true
            }

            R.id.action_products -> {
                val intent = Intent(this@MainActivity, ProductActivity::class.java)
                startActivity(intent)
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }
}
