package com.axial.billingcenter.Activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.axial.billingcenter.Adapters.BillAdapter
import com.axial.billingcenter.Entities.Debtor
import com.axial.billingcenter.Entities.Product
import com.axial.billingcenter.R
import com.beust.klaxon.Klaxon
import kotlinx.android.synthetic.main.activity_bill.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_product.*
import kotlinx.android.synthetic.main.activity_product.toolbar
import kotlinx.android.synthetic.main.content_product.*
import kotlinx.android.synthetic.main.content_product.purchasePriceTitle
import kotlinx.android.synthetic.main.content_product.salePriceTitle

class BillActivity : AppCompatActivity() {

    var debtor: Debtor? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bill)

        debtor = Klaxon().parse(intent.getStringExtra("debtor"))

        purchasePriceTitle.text = "Unidades"
        salePriceTitle.text = "Total"

        var adapter = BillAdapter(this)
        var recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter.setProducts(Klaxon().parseArray(debtor!!.products))

        totalBill.text = "Total: $" + debtor!!.bill.toInt()
    }
}
