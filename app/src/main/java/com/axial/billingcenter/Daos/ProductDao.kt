package com.axial.billingcenter.Daos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.axial.billingcenter.Entities.Product

@Dao
interface ProductDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(product: Product)

    @Query("SELECT * from product")
    fun getAllProducts(): LiveData<List<Product>>

    @Query("Select sold_units from product WHERE id like :id")
    fun getSoldUnidsById(id: Int): Int

    @Query("DELETE FROM product")
    fun deleteAllProducts()

    @Update
    fun updateProduct(product: Product)
}