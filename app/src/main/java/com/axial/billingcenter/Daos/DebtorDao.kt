package com.axial.billingcenter.Daos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.axial.billingcenter.Entities.Debtor

@Dao
interface DebtorDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(debtor: Debtor)

    @Query("SELECT * from debtor")
    fun getAll(): LiveData<List<Debtor>>

    @Query("DELETE FROM debtor")
    fun deleteAllDebtors()

    @Update
    fun updateDebtor(debtor: Debtor)

    //@Query("SELECT * from debtor WHERE id LIKE :id")
    //fun getProducts(id: Int): LiveData<String>
}